---
sidebar_position: 2
---

# Intégrer le projet sous Eclipse

---

## Étape 1 : Ouvrir le workspace créé précédemment

Lancer Eclipse IDE et choisir votre `workspace`. 

![Workspace Eclipse](./img/eclipse-workspace.png)

## Étape 2 : Importer le projet Spring

Cliquer sur `Import projects` : 

![Import](./img/import.png)

Choisir le dossier **Maven** :

![Maven](./img/import-maven.png)

Puis choisir `Existing Maven Projects` et cliquer sur **Next**.

![Exist Maven](./img/existing-maven.png)

Une nouvelle fenêtre s'affiche ! Il suffit de choisir le projet spring généré auparavant. Pour se faire il suffit de cliquer sur **Browse**. 

![Browse](./img/browse.png)

Sélectionner le dossier `slamProject`.

![Select](./img/select.png)

Enfin cliquer sur finish en vérifiant la présence du `pom.xml`.

![Finish](./img/finish.png)

Bravo le projet devrait être intégré sous Eclipse !

![Valid](./img/validate.png)