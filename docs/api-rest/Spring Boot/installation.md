---
sidebar_position: 1
---


# Installation

---

:::info

Pour information notre éditeur sera : [Eclipse IDE](https://www.eclipse.org/downloads/)

:::

## Étape 1 : Spring Initializr

Dans un premier temps il faut se rendre sur [Spring Boot Initializr](https://start.spring.io).

![Spring Boot Initializr site](./img/site-initialize.png)

## Étape 2 : Choix du projet et du langage

Celui qui va nous intéresser c'est : [Maven](https://maven.apache.org)

![Projet Maven les gars](./img/project.png)

Le projet sera réalisé en : [Java](https://www.java.com/fr/)

![Java](./img/language.png)

## Étape 3 : Choix de la version de Spring Boot

Nous allons utiliser la version **par défaut** de Spring Boot. Dans le cas présent nous allons prendre la version `3.2.0`

![Version Spring](./img/version-spring.png)

## Étape 4 : Les métadonnées du projet

Le choix du `Group`, de l'`Artifact`, du `Name` et du `Package` va nous permettre de nommer les parties du projet.

Il faut également choisir le packaging `Jar` puis la version de `Java` que vous avez installé.

![Metadata](./img/metadata.png)

:::tip

Pour vérifier la version de votre java tapez dans une invite de commande : 

```
java -version
```

:::

## Étape 5 : Les dépendances lié au projet de l'API REST

Pour notre projet nous aurons besoin des dépendances suivantes :

![Dependencies](./img/dependencies.png)

:::tip

Pour ajouter une dépendance il suffit de cliquer sur le boutton `ADD DEPENDENCIES` puis rechercher par *nom*.
![button Dependencies](./img/dep-button.png)
![Search Dependencies](./img/search-dep.png)

:::

## Étape 6 : Générer

Plus qu'à **générer** !

![Generate](./img/generate.png)

Une fois généré, il faut créer un **workspace** dédié au projet puis extraire le zip Spring Boot dans ce répertoire.

![workspace](./img/workspace.png)

:::note
Le nom qu'on a donné dans Spring Boot correspond au dossier extrait ici dans le `workspaceSpring`

![workspace-extract](./img/extract-project.png)

:::

