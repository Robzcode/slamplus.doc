import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Besoin d\'aide en dev ?',
    Svg: require('@site/static/img/helping.svg').default,
    description: (
      <>
        Si tu cherche une documentation sur plusieurs sujets en développement, SLAM+ est fait pour toi !
      </>
    ),
  },
  {
    title: 'Projet étudiant',
    Svg: require('@site/static/img/student.svg').default,
    description: (
      <>
        Il s'agit d'un projet personnel étudiant, ce qui signifie que cette documentation est à la fois une base de connaissance.
        Donc n'est pas spécialement parfait, mais en constante évolution.
      </>
    ),
  },
  {
    title: 'J\'accède à CYBER+',
    Svg: require('@site/static/img/cyber.svg').default,
    description: (
      <>
        CYBER+ reste le même projet, mais orienté sur la cybersécurité en générale !
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
